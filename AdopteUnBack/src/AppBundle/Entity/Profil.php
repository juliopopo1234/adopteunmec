<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Profil
 *
 * @ORM\Table(name="profil")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProfilRepository")
 */
class Profil
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     */
    private $firstName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="fb", type="string", length=255, nullable=true)
     */
    private $fb;

    /**
     * @var string
     *
     * @ORM\Column(name="lkdn", type="string", length=255, nullable=true)
     */
    private $lkdn;

    /**
     * @var string
     *
     * @ORM\Column(name="twtr", type="string", length=255, nullable=true)
     */
    private $twtr;

    /**
     * @var string
     *
     * @ORM\Column(name="insta", type="string", length=255, nullable=true)
     */
    private $insta;

    /**
     * @var string
     *
     * @ORM\Column(name="github", type="string", length=255, nullable=true)
     */
    private $github;

    /**
     * @var string
     *
     * @ORM\Column(name="gitlab", type="string", length=255, nullable=true)
     */
    private $gitlab;

    /**
     * @var string
     *
     * @ORM\Column(name="so", type="string", length=255, nullable=true)
     */
    private $so;

    /**
     * @var array
     *
     * @ORM\Column(name="other", type="json_array", nullable=true)
     */
    private $other;

    /**
     * @var string
     *
     * @ORM\Column(name="pdfCv", type="string", length=255, nullable=true)
     */
    private $pdfCv;

    /**
     * @var string
     *
     * @ORM\Column(name="linkCv", type="string", length=255, nullable=true)
     */
    private $linkCv;

    /**
     * @var string
     *
     * @ORM\Column(name="video", type="string", length=255, nullable=true)
     */
    private $video;

    /**
     * @var bool
     *
     * @ORM\Column(name="adopted", type="boolean")
     */
    private $adopted;

    /**
     * @var string
     *
     * @ORM\Column(name="promo", type="string", length=255)
     */
    private $promo;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="profil", cascade={"persist"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="Profil_Badge", mappedBy="profil")
     */
    private $profilBadges;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="profil")
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="profil")
     */
    private $projects;

    /**
     * @var string
     *
     * @ORM\Column(name="intro", type="text", nullable=true)
     */
    private $intro;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="img", type="string", length=255, nullable=true)
     */
    private $img;

    public function __construct()
    {
        $this->projects = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function jsonSerialize()
    {
        $array = array(
            'firstName' => $this->firstName,
            'lastName'=> $this->lastName,
            'contact' => [
                'phone' => $this->phone,
                'birthday' => $this->birthday->format('d/m/Y'),
                'city' => $this->city
            ],
            'socials' => [
                'fb' => $this->fb,
                'lkdn' => $this->lkdn,
                'twtr' => $this->twtr,
                'insta' => $this->insta,
                'github' => $this->github,
                'gitlab' => $this->gitlab,
                'so' => $this->so,
                'other' => $this->other,
            ],
            'medias' => [
                'video' => $this->video,
                'pdfCv' => $this->pdfCv,
                'linkCv' => $this->linkCv,
            ],
            'adopted' => $this->adopted,
            'promo' => $this->promo,
            'intro' => $this->intro,
            'img' => $this->img,
        );
        if($this->user != null)
            $array['contact']['mail'] = $this->user->getEMail();
        return $array;
    }

    public function __toString() {
        return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Profil
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Profil
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return Profil
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Get birthdayFormat
     * 
     * @return string
     */
    public function getBirthdayFormat()
    {
        return $this->birthday->format('d/m/Y');
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Profil
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set fb
     *
     * @param string $fb
     *
     * @return Profil
     */
    public function setFb($fb)
    {
        $this->fb = $fb;

        return $this;
    }

    /**
     * Get fb
     *
     * @return string
     */
    public function getFb()
    {
        return $this->fb;
    }

    /**
     * Set lkdn
     *
     * @param string $lkdn
     *
     * @return Profil
     */
    public function setLkdn($lkdn)
    {
        $this->lkdn = $lkdn;

        return $this;
    }

    /**
     * Get lkdn
     *
     * @return string
     */
    public function getLkdn()
    {
        return $this->lkdn;
    }

    /**
     * Set twtr
     *
     * @param string $twtr
     *
     * @return Profil
     */
    public function setTwtr($twtr)
    {
        $this->twtr = $twtr;

        return $this;
    }

    /**
     * Get twtr
     *
     * @return string
     */
    public function getTwtr()
    {
        return $this->twtr;
    }

    /**
     * Set insta
     *
     * @param string $insta
     *
     * @return Profil
     */
    public function setInsta($insta)
    {
        $this->insta = $insta;

        return $this;
    }

    /**
     * Get insta
     *
     * @return string
     */
    public function getInsta()
    {
        return $this->insta;
    }

    /**
     * Set github
     *
     * @param string $github
     *
     * @return Profil
     */
    public function setGithub($github)
    {
        $this->github = $github;

        return $this;
    }

    /**
     * Get github
     *
     * @return string
     */
    public function getGithub()
    {
        return $this->github;
    }

    /**
     * Set gitlab
     *
     * @param string $gitlab
     *
     * @return Profil
     */
    public function setGitlab($gitlab)
    {
        $this->gitlab = $gitlab;

        return $this;
    }

    /**
     * Get gitlab
     *
     * @return string
     */
    public function getGitlab()
    {
        return $this->gitlab;
    }

    /**
     * Set so
     *
     * @param string $so
     *
     * @return Profil
     */
    public function setSo($so)
    {
        $this->so = $so;

        return $this;
    }

    /**
     * Get so
     *
     * @return string
     */
    public function getSo()
    {
        return $this->so;
    }

    /**
     * Set other
     *
     * @param array $other
     *
     * @return Profil
     */
    public function setOther($other)
    {
        $this->other = $other;

        return $this;
    }

    /**
     * Get other
     *
     * @return array
     */
    public function getOther()
    {
        return $this->other;
    }

    /**
     * Set pdfCv
     *
     * @param string $pdfCv
     *
     * @return Profil
     */
    public function setPdfCv($pdfCv)
    {
        $this->pdfCv = $pdfCv;

        return $this;
    }

    /**
     * Get pdfCv
     *
     * @return string
     */
    public function getPdfCv()
    {
        return $this->pdfCv;
    }

    /**
     * Set linkCv
     *
     * @param string $linkCv
     *
     * @return Profil
     */
    public function setLinkCv($linkCv)
    {
        $this->linkCv = $linkCv;

        return $this;
    }

    /**
     * Get linkCv
     *
     * @return string
     */
    public function getLinkCv()
    {
        return $this->linkCv;
    }

    /**
     * Set video
     *
     * @param string $video
     *
     * @return Profil
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set adopted
     *
     * @param boolean $adopted
     *
     * @return Profil
     */
    public function setAdopted($adopted)
    {
        $this->adopted = $adopted;

        return $this;
    }

    /**
     * Get adopted
     *
     * @return bool
     */
    public function getAdopted()
    {
        return $this->adopted;
    }

    /**
     * Set promo
     *
     * @param string $promo
     *
     * @return Profil
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;

        return $this;
    }

    /**
     * Get promo
     *
     * @return string
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Profil
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add comment
     *
     * @param \AppBundle\Entity\Comment $comment
     *
     * @return Profil
     */
    public function addComment(\AppBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \AppBundle\Entity\Comment $comment
     */
    public function removeComment(\AppBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Add project
     *
     * @param \AppBundle\Entity\Project $project
     *
     * @return Profil
     */
    public function addProject(\AppBundle\Entity\Project $project)
    {
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove project
     *
     * @param \AppBundle\Entity\Project $project
     */
    public function removeProject(\AppBundle\Entity\Project $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * Get projects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * Set intro
     *
     * @param string $intro
     *
     * @return Profil
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;

        return $this;
    }

    /**
     * Get intro
     *
     * @return string
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * Add profilBadge
     *
     * @param \AppBundle\Entity\Profil_Badge $profilBadge
     *
     * @return Profil
     */
    public function addProfilBadge(\AppBundle\Entity\Profil_Badge $profilBadge)
    {
        $this->profilBadges[] = $profilBadge;

        return $this;
    }

    /**
     * Remove profilBadge
     *
     * @param \AppBundle\Entity\Profil_Badge $profilBadge
     */
    public function removeProfilBadge(\AppBundle\Entity\Profil_Badge $profilBadge)
    {
        $this->profilBadges->removeElement($profilBadge);
    }

    /**
     * Get profilBadges
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProfilBadges()
    {
        return $this->profilBadges;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Profil
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set img
     *
     * @param string $img
     *
     * @return Profil
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Get img
     *
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }
}
